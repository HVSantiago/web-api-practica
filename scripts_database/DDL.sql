CREATE DATABASE Practica2017;
USE Practica2017;

/*
LAS INSTRUCCIONES ESTAN EN TODO EL DOCUMENTO DDL.
1. NO SE DEBE DE ALTERAR LA ESTRUCTURA DE LA BASE DE DATOS DADA.
2. CREAR EL ARCHIVO CORRESPONDIENTE DE DML PARA LA BASE DE DATOS.
3. EN EL ARCHIVO DML UTILIZAR TODOS LOS PROCEDIMIENTOS DECLARADOS EN EL ARCHIVO DDL.
4. SOLO DE DEBE DE CREAR LOS PROCEDIMIENTOS DE ALMACENADO INDICADOS.
*/


CREATE TABLE Tarea(
		idTarea INT NOT NULL AUTO_INCREMENT,
    titulo VARCHAR(30) NOT NULL,
    descripcion VARCHAR(40) NOT NULL,
		fecha_registro DATETIME NOT NULL,
		fecha_final DATETIME NOT NULL,
    PRIMARY KEY (idTarea)
);

CREATE TABLE Usuario(
		idUsuario INT NOT NULL AUTO_INCREMENT,
    nick VARCHAR(30) NOT NULL,
    contrasena VARCHAR(30) NOT NULL,
    cambios_contrasena INT NOT NULL,
		fecha_registro DATETIME NOT NULL,
    fecha_modificacion DATETIME NULL,
		picture TEXT NULL,
    PRIMARY KEY (idUsuario)
);

CREATE TABLE UsuarioTareas(
		idUsuarioTareas INT NOT NULL AUTO_INCREMENT,
    idUsuario INT NOT NULL,
    idTarea INT NOT NULL,
    PRIMARY KEY (idUsuarioTareas),
    FOREIGN KEY (idUsuario) REFERENCES Usuario(idUsuario),
    FOREIGN KEY (idTarea) REFERENCES Tarea(idTarea)
);

/*
SP USUARIO INSERT
*/
DELIMITER $$
CREATE PROCEDURE sp_insertUsuario(
_nick VARCHAR(30),
_contrasena VARCHAR(30))
BEGIN
	INSERT INTO Usuario(nick, contrasena, cambios_contrasena, fecha_registro, fecha_modificacion, picture)
    VALUES(_nick, _contrasena, 0, NOW(), NOW(), NULL);
END $$
DELIMITER ;

/*
SP USUARIO UPDATE
*/
DELIMITER $$
CREATE PROCEDURE sp_updateUsuario
(IN _idUsuario INT, IN _nick VARCHAR(50), IN _contrasena VARCHAR(50), IN _picture TEXT)
BEGIN
	DECLARE _obtenerCambios INT;
	DECLARE _sumarCambio INT;
	SET _obtenerCambios = (SELECT cambios_contrasena FROM Usuario WHERE idUsuario = _idUsuario);
	SET _sumarCambio = _obtenerCambios + 1;
  UPDATE Usuario SET nick = _nick, contrasena = _contrasena, cambios_contrasena = _sumarCambio,
	fecha_modificacion = NOW(), picture = _picture WHERE idUsuario = _idUsuario;
END;
$$

/*
SP_AUTENTICAR
*/
DELIMITER $$
CREATE PROCEDURE sp_autenticarUsuario(
_nick VARCHAR(30),
_contrasena VARCHAR(30))
BEGIN
	SELECT * FROM Usuario WHERE nick = _nick AND contrasena = _contrasena LIMIT 1;
END $$
DELIMITER ;

/*
SP USUARIO SELECT
*/
DELIMITER $$
CREATE PROCEDURE sp_selectUsuarios
()
BEGIN
  SELECT idUsuario, nick, contrasena, cambios_contrasena,
	DATE_FORMAT(fecha_registro, '%y-%c-%d') AS 'fecha_registro',
	DATE_FORMAT(fecha_registro, '%h:%i:%s') AS 'hora_registro',
	DATE_FORMAT(fecha_modificacion, '%y-%c-%d') AS 'fecha_modificacion',
	DATE_FORMAT(fecha_modificacion, '%h:%i:%s') AS 'hora_modificacion',
	picture
	 FROM Usuario ORDER BY(DATE_FORMAT(fecha_modificacion, '%y-%c-%d %h:%i:%s')) DESC;
END;
$$

/*
SP USUARIO DELETE
*/

DELIMITER $$
CREATE PROCEDURE sp_deleteUsuario
(IN _idUsuario INT)
BEGIN
	DELETE Tarea
	FROM Tarea t
		INNER JOIN UsuarioTareas ut ON t.idTarea = ut.idTarea
        INNER JOIN Usuario u ON ut.idUsuario = u.idUsuario
	WHERE ut.idUsuario = _idUsuario;
    DELETE FROM UsuarioTareas WHERE idUsuario = _idUsuario;
    DELETE FROM Usuario WHERE idUsuario = _idUsuario;
END;
$$


/*
SP TAREA INSERT
*/
DROP PROCEDURE IF EXISTS sp_insertTarea
DELIMITER $$
CREATE PROCEDURE sp_insertTarea
  (IN _idUsuario INT, IN _titulo VARCHAR(40), _descripcion TEXT, _fecha_final DATETIME)
BEGIN
  DECLARE _obtenerId INT;

  INSERT INTO Tarea(titulo, descripcion, fecha_registro, fecha_final)
  VALUES (_titulo, _descripcion, NOW(), _fecha_final);

  SET _obtenerId = (SELECT MAX(idTarea) FROM Tarea);
  INSERT INTO UsuarioTareas(idUsuario, idTarea)
  VALUES(_idUsuario, _obtenerId);
END;
$$

/*
SP TAREA SELECT
*/
DELIMITER $$
CREATE PROCEDURE sp_selectTareas
(IN _idUsuario INT)
BEGIN
  SELECT ut.idTarea, t.titulo, t.descripcion,
	DATE_FORMAT(t.fecha_registro, '%y-%c-%d') AS 'fecha_registro',
 	DATE_FORMAT(t.fecha_registro, '%h:%i:%s') AS 'hora_registro',
 	DATE_FORMAT(t.fecha_final, '%y-%c-%d') AS 'fecha_final',
	DATE_FORMAT(t.fecha_final, '%h:%i:%s') AS 'hora_final'
  FROM UsuarioTareas ut
    INNER JOIN Tarea t ON ut.idTarea = t.idTarea
  WHERE ut.idUsuario = _idUsuario
  ORDER BY(DATE_FORMAT(fecha_final, '%y-%c-%d %h:%i:%s')) DESC;
END;
$$
