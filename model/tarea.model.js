var database = require('../config/database.config');
var tarea = {}

tarea.selectAll = function (idUsuario, callback) {
  if (database) {
    database.query('CALL sp_selectTareas(?)', idUsuario,
    function (error, resultados) {
      if (error) {
          throw error;
      } else {
          callback(resultados);
      }
    });
  }
}


tarea.insert = function(data, callback) {
  if(database) {
    database.query("CALL sp_insertTarea(?,?,?,?)",
    [data.idUsuario, data.titulo, data.descripcion, data.fecha_final],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback({"affectedRows": resultado.affectedRows});
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

module.exports = tarea;
